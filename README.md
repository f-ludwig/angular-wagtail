# Angular Wagtail

Integrate a "headless" Wagtail CMS with Angular

## Features

- Match Wagtail Page Types to Angular Components
  - Or to lazy loaded modules
- Support Wagtail's preview feature thanks to wagtail-headless-preview
- Support Wagtail's SEO meta tag features
- Support for Angular Universal
  - Supports proper 404, 301, and 302 status codes from Node
- Show custom 404 page when no CMS page
- Keep an index of all wagtail pages or exclude certain types
- Support Multiple Wagtail sites

## Assumptions

- Assumes Wagtail and Angular are separate and that Angular is running as a single page application

# Installation

Consider reading the getting started blog post [here](https://davidmburke.com/2019/10/20/angular-wagtail-1-0-and-getting-started/).

1. Install `angular-wagtail` with npm or yarn.
2. Install `@nguniversal/express-engine` even if not using angular-universal. Angular Wagtail uses some injection tokens from it.
3. Add this catchall route to your Routes `{ path: "**", component: CMSLoaderComponent, canActivate: [CMSLoaderGuard] }` imported from `angular-wagtail`
4. Add WagtailModule.forRoot({}) to your root AppModule.
5. For preview support, add a wrapper module and route:

preview.module.ts

```typescript
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PreviewModule as PreviewModuleBase, PreviewComponent } from "angular-wagtail";

@NgModule({
    imports: [
        PreviewModuleBase,
        RouterModule.forChild([{path: "", component: PreviewComponent}])
    ]
})
export class PreviewModule {}
```

route:
```typescript
{ path: "preview", loadChildren: () => import("angular-wagtail/preview.module").then(m => m.PreviewModule)}
```

The path may be anything, but it needs to match your Django server's HEADLESS_PREVIEW_CLIENT_URLS path

6. If using Universal, ensure app.server.module.ts has `ServerTransferStateModule`
7. Configure WagtailModule.forRoot, which accepts the following configuration object.

- pagesApiPath - Set this to the path (without domain) of your wagtail pages API endpoint. For example /api/v2/pages/ (default)
- pagesDraftApiPath - (Optional) Set this only if you'd like to override pagesApiPath when using the draft feature. This can be useful when used with a CDN where you may want to bypass cache.
- notFoundComponent - A component to display when a page 404's.
- wagtailSiteId - Filter only by this wagtail site. Alternatively, if using Node, set the environment variable `WAGTAIL_SITE_ID`.
- excludeApiTypes - Wagtail Page types to exclude. If you had 1020 pages and 1000 of them were blog posts, you could exclude the blog posts.I alwa
- pageTypes - An array of objects that match Wagtail Page Types to Angular Components. It's similar to Angular Router. Example:

```typescript
[{
    type: 'cms.MyGreatPage', component: MyGreatComponent,
    type: 'cms.LazyLoadedPage', loadChildren: './lazy-loaded/lazy.module#LazyModule'
}]
```

- setCanonicalURL = (Optional) Set to `true` to have Angular set the Canonical URL link tag to the wagtail page's html_url. You may need to force Wagtail to show the html_url with a https by setting the Wagtail Site's port to 443.

## Lazy Loaded Modules Support

Read this if you use lazy loaded modules with multiple route-able components.

If you have lazy loaded modules with just one route-able component it will just work. You lazy module's routing might look like this:

```typescript
const routes: Routes = [
  {
    path: '',
    component: FooComponent
  }
];
```

Angular supports multiple child routes in a lazy loaded module. Supporting this in Angular Wagtail requires additional configuration to deal with our routes not being known ahead of time.

1. Every page type with lazy loaded component must be declared in app.module

```typescript
    WagtailModule.forRoot({
      pageTypes: [
        {
          type: "sandbox.FooPage",
          loadChildren: () =>
            import("./lazy/lazy.module").then(mod => mod.LazyModule)
        },
        {
          type: "sandbox.BarPage",
          loadChildren: () =>
            import("./lazy/lazy.module").then(mod => mod.LazyModule)
        }
      ],
```

Notice how both FooPage and BarPage load the LazyModule.

2. The lazy loaded module requires a WagtailModule.forFeature import. Add any route-able components to "entryComponents". It also requires a special coalescingResolver function in it's constructor. coalescingResolver allows the AppModule to reference components from another module. This probably can go away in Angular 9. More info [here](https://github.com/angular/angular/issues/14324#issuecomment-481898762).

```typescript
import { NgModule, ComponentFactoryResolver } from "@angular/core";
import { CommonModule } from "@angular/common";

import { LazyComponent } from "./lazy.component";
import { AnotherLazyComponent } from "./another-lazy.component";
import { WagtailModule, CoalescingComponentFactoryResolver } from "angular-wagtail";

@NgModule({
  entryComponents: [AnotherLazyComponent, LazyComponent],
  declarations: [AnotherLazyComponent, LazyComponent],
  imports: [
    CommonModule,
    WagtailModule.forFeature([
      {
        component: AnotherLazyComponent,
        type: "sandbox.BarPage"
      },
      {
        component: LazyComponent,
        type: "sandbox.FooPage"
      }
    ])
  ]
})
export class LazyModule {
  constructor(
    coalescingResolver: CoalescingComponentFactoryResolver,
    localResolver: ComponentFactoryResolver
  ) {
    coalescingResolver.registerResolver(localResolver);
  }
}
```

Finally, if have a empty string path in your router, you need to work around this [Angular Router bug](https://github.com/angular/angular/issues/32852). There are a number of workarounds. Serve your home page in Angular Wagtail config instead of Angular Router. Or the provided `RouterModule.forChild([])` workaround in the bug report.

3. (if using Angular Universal) Lazy loaded the module server side will not work. In your app.server.module.ts, add all lazy loaded modules to imports. This will have no negative performance impact. Your browser bundle will continue to lazy load modules while your server bundle will load all modules at once.

### Fetching CMS Data with a resolver

Angular Router's resolve feature allows sites to fetch data before completing a route. In your route or WagtailModule pageTypes add

```typescript
    // ...
    component: YourComponent,
    resolve: {
        cmsData: GetPageDataResolverService,
    },
    runGuardsAndResolvers: 'always',
},
```

Then in the component's ngOnInit add

```
cmsData$: Observable<YourPageDetails>;

ngOnInit() {
    this.cmsData$ = this.route.data.pipe(map(dat => dat.cmsData));
```

### Using NGRX Router Store?

You will encounter this upstream [bug](https://github.com/ngrx/platform/issues/1781). As it's a bug in ngrx, we cannot fix it here. You can define a custom router serializer to avoid it.

```typescript
export interface CustomRouterState {
    url: string;
    params: Params;
    queryParams: Params;
    data: Data;
}
export class CustomRouterStateSerializer
    implements RouterStateSerializer<CustomRouterState> {
    serialize(routerState: RouterStateSnapshot): CustomRouterState {
        let route = routerState.root;

        while (route.firstChild) {
            route = route.firstChild;
        }

        const state: CustomRouterState = {
            url: routerState.url || '/', // this is the important line
            params: route.params,
            queryParams: routerState.root.queryParams,
            data: route.data,
        };
        return state;
    }
}

// ...

StoreRouterConnectingModule.forRoot({
    serializer: CustomRouterStateSerializer,
}),
```

## SSR Node status code support

If running your SSR server from Node, enable 404 and 30x status codes by editing server.ts. Replace

```typescript
app.get('*', (req, res) => {
    res.render('index', { req });
});
```

with

```typescript
import { REQUEST, RESPONSE } from "@nguniversal/express-engine/tokens";

// ...

app.get("*", (req, res) => {
  res.render("index", {
    req,
    res,
    providers: [
      {
        provide: REQUEST,
        useValue: req
      },
      {
        provide: RESPONSE,
        useValue: res
      }
    ]
  });
});
```

## Environment Variables

If using Angular Universal and serving from node - you may control some configuration variables via environment variables.

- `CMS_DOMAIN` - The domain of the CMS to use. For example cms.example.com
- `WAGTAIL_SITE_ID` - Overrides wagtailSiteId forcing Node to always use this ID. This can be useful as Node isn't easily able to determine the user's domain and Angular Universal does not support doing so.

Backend instructions coming soon.

## What if I need to do something after Wagtail Detail page data loads?

For example, what if you are using an A/B testing tool that needs to trigger something after a page "settles" or finishes getting CMS data.
WagtailService.cmsDetailPageSuccess\$ is an observable that can be subscribed to so that any such event can occur. For example:

`this.wagtailService.cmsDetailPageSuccess$.subscribe(() => console.log('data is loaded'));`

## sitemap.xml support

Wagtail SPA Integration comes with a extended wagtail sitemap.xml feature to explicitly set the site by id. This can be called from the SSR server.ts file.

```typescript
const request = require('request');
const WAGTAIL_SITE_ID = process.env.WAGTAIL_SITE_ID || null;
const CMS_DOMAIN = process.env.CMS_DOMAIN || null;

app.get('/sitemap.xml', (req, res) => {
    if (WAGTAIL_SITE_ID && CMS_DOMAIN) {
        const params = {
            site: WAGTAIL_SITE_ID,
        };
        const url = CMS_DOMAIN + '/sitemap.xml';
        request({ url: url, qs: params }, function(err, response, body) {
            if (err) {
                console.log(err);
                return;
            }
            res.setHeader('content-type', 'application/xml');
            res.send(body);
        });
    }
});
```

# How does it work

- When no existing Angular router path matches, get a list of all wagtail pages.
  - Pass this list from node to the client via transfer state.
- Use this listing to add to Angular Router routes.
- Check if the current path is one of these routes.
  - If yes, redirect to it (causes the component to display)
- If the current path is not found, check if it's a Wagtail redirect
  - Go to the redirect when available
- If no matching page or redirect is found - display a not found component, or blank page if not provided.

# Developing this module

There is a simple Angular application provided for testing and development.
It assumes the wagtail server will be on `localhost:8000` but this can be changed by editing `proxy.conf.json`.

- Start your wagtail server with wagtail-spa-integration installed.
- `npm i`
- `npm start`

## Build

`ng build angular-wagtail`

## Test

`ng test angular-wagtail`
