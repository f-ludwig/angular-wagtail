import {
  Component,
  OnInit,
  ViewChild,
  ViewContainerRef,
  ComponentRef,
  ComponentFactoryResolver,
  Inject,
  PLATFORM_ID
} from "@angular/core";
import { isPlatformServer } from "@angular/common";
import { Router } from "@angular/router";
import { tap, catchError } from "rxjs/operators";
import { EMPTY } from "rxjs";
import { WagtailService } from "./wagtail.service";
import { StatusCodeService } from "./status-code.service";
import { IWagtailRedirect } from "./interfaces";

@Component({
  template: `
    <div #target></div>
  `
})
export class CMSLoaderComponent implements OnInit {
  constructor(
    private wagtail: WagtailService,
    private componentFactoryResolver: ComponentFactoryResolver,
    private router: Router,
    @Inject(PLATFORM_ID) private platformId: Object,
    private statusCodeService: StatusCodeService
  ) {}
  @ViewChild("target", { read: ViewContainerRef, static: true })
  target: ViewContainerRef;
  cmpRef: ComponentRef<Component>;

  ngOnInit() {
    this.wagtail
      .getRedirect()
      .pipe(
        tap(redirects => {
          if (redirects.length > 0) {
            this.redirect(redirects[0]);
          } else {
            this.display404();
          }
        }),
        catchError(() => {
          this.display404();
          return EMPTY;
        })
      )
      .toPromise();
  }

  /**
   * On server, send redirect status code
   * On browser, change window.location
   */
  redirect(redirect: IWagtailRedirect) {
    if (isPlatformServer(this.platformId)) {
      this.statusCodeService.redirect(redirect.link, redirect.is_permanent);
    } else {
      const path = redirect.link;
      if (path.startsWith("http")) {
        if (window.location.href === path) {
          console.warn("Refused to redirect to self");
        } else {
          window.location.href = path;
        }
      } else {
        this.router.navigate([path]);
      }
    }
  }

  /**
   * Replace component content with defined not found component
   * On server, set 404 status code
   */
  display404() {
    if (this.wagtail.notFoundComponent) {
      const factory = this.componentFactoryResolver.resolveComponentFactory(
        this.wagtail.notFoundComponent
      );
      this.cmpRef = this.target.createComponent(factory);
      if (isPlatformServer(this.platformId)) {
        this.statusCodeService.setStatus(404, "Not Found");
      }
    }
  }
}
