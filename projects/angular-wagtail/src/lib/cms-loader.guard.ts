import { Injectable } from "@angular/core";
import {
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  CanActivate,
  Router
} from "@angular/router";
import { tap, map } from "rxjs/operators";
import { WagtailService } from "./wagtail.service";

let firstLoad = true;

@Injectable({
  providedIn: "root"
})
export class CMSLoaderGuard implements CanActivate {
  redirects = 0;
  constructor(private router: Router, private service: WagtailService) {
    const subscription = router.events.subscribe(e => {
      if ((e as any).routerEvent) {
        firstLoad = false;
        subscription.unsubscribe();
      }
    });
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    this.redirects = this.redirects + 1;
    setTimeout(() => (this.redirects = 0), 50);
    if (this.redirects < 5) {
      return this.service.matchRouteToComponent(state.url).pipe(
        tap(wasFound => {
          if (wasFound) {
            this.router.navigate([this.service.sanitizePath(state.url)], {
              queryParamsHandling: "merge",
              queryParams: route.queryParams,
              fragment: route.fragment,
              replaceUrl: firstLoad
            });
          }
        }),
        map(wasFound => !wasFound)
      );
    }
    console.error("Angular Wagtail: Redirect loop detected");
    return false;
  }
}
