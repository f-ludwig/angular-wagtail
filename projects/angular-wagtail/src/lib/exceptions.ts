class AngularWagtailError extends Error { }

export class NotFoundError extends AngularWagtailError {
    constructor() {
        super();
        Object.setPrototypeOf(this, NotFoundError.prototype);
    }
}

export class PageDetailNotFound extends AngularWagtailError {
    constructor() {
        super();
        Object.setPrototypeOf(this, PageDetailNotFound.prototype);
    }
}
