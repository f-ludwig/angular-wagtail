import { Type } from "@angular/core";
import { LoadChildren, ResolveData, RunGuardsAndResolvers, Data } from "@angular/router";

interface IWagtailMeta {
  type: string;
  detail_url: string;
  html_url: string;
  slug: string;
  first_published_at?: string;
}

export interface IWagtailMetaDetail extends IWagtailMeta {
  show_in_menus: boolean;
  seo_title: string;
  search_description: string;
  parent: IWagtailPage | null;
}

interface IWagtailPageBase {
  id: number;
  title: string;
}

export interface IWagtailPage extends IWagtailPageBase {
  meta: IWagtailMeta;
}

export interface IWagtailPageDetail extends IWagtailPageBase {
  meta: IWagtailMetaDetail;
}

interface IWagtailResponseMeta {
  total_count: number;
}

export interface IWagtailResponse {
  meta: IWagtailResponseMeta;
  items: IWagtailPage[];
}

export interface IPageTypes {
  type: string;
  component?: Type<any>;
  data?: Data;
  loadChildren?: LoadChildren;
  resolve?: ResolveData;
  runGuardsAndResolvers?: RunGuardsAndResolvers;
}

export interface IWagtailModuleConfig {
  /* The url path of the wagtail v2 pages API endpoint. Defaults to /api/v2/pages/ */
  pagesApiPath?: string;
  /* Optional, set this to override the pagesApiPath when viewing draft. Could be useful to bypass cache */
  pagesDraftApiPath?: string;
  /* Matches wagtail page types to router components or router loadChildren */
  pageTypes: IPageTypes[];
  /* Show this component when page is not found. If not set, router will show empty template. */
  notFoundComponent?: Type<any>;
  /*
  Limit the amount of pages returned by the wagtail api. Defaults to 50.
  Should not be above wagtail WAGTAILAPI_LIMIT_MAX
  */
  pagesApiLimit?: number;
  /* Sets the fields param for the wagtail pages api. Defaults to -first_published_at */
  pagesApiFields?: string;
  /* Exclude page type. Example: myblog.BlogPost */
  excludeApiTypes?: string;
  /* Optional, limit queries to this wagtail site ID */
  wagtailSiteId?: number;
  /* Optional, use this as the wagtail domain */
  wagtailSiteDomain?: string;
  /* Set canonical URL from wagtail page settings */
  setCanonicalURL?: boolean;
}

export interface IWagtailRedirect {
  old_path: string;
  is_permanent: boolean;
  site: number | null;
  link: string;
}
