import { Inject, Injectable, Optional } from "@angular/core";
import { RESPONSE } from "@nguniversal/express-engine/tokens";

@Injectable()
export class StatusCodeService {
  constructor(
    @Optional()
    @Inject(RESPONSE)
    private _response: any
  ) {}

  setStatus(code: number, message: string): void {
    if (this._response) {
      this._response.statusCode = code;
      this._response.statusMessage = message;
    }
  }

  redirect(url: string, isPermanant = true) {
    if (isPermanant) {
      this.setStatus(301, "Moved Permanently");
    } else {
      this.setStatus(302, "Found");
    }
    this._response.setHeader("Location", url);
  }
}
