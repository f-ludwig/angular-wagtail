import { InjectionToken } from "@angular/core";

export const WAGTAIL_CONFIG = new InjectionToken(
  "angular-wagtail: Wagtail Config"
);

export const FEATURE_PAGE_TYPES = new InjectionToken<any[][]>(
  "angular-wagtail: Feature Page Types"
);
