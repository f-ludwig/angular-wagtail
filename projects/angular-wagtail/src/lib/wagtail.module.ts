import { NgModule, ModuleWithProviders } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RouterModule, Router } from "@angular/router";
import { BrowserTransferStateModule } from "@angular/platform-browser";
import { HttpClientModule } from "@angular/common/http";
import { WagtailService } from "./wagtail.service";
import { StatusCodeService } from "./status-code.service";
import { IWagtailModuleConfig, IPageTypes } from "./interfaces";
import { CMSLoaderComponent } from "./cms-loader.component";
import { FEATURE_PAGE_TYPES, WAGTAIL_CONFIG } from "./tokens";
import { FeatureRoutesModule } from "./feature-routes.module";
import { CoalescingComponentFactoryResolver } from "./coalescing-component-factory-resolver.service";

@NgModule({
  declarations: [CMSLoaderComponent],
  entryComponents: [CMSLoaderComponent],
  imports: [
    CommonModule,
    RouterModule,
    HttpClientModule,
    BrowserTransferStateModule
  ],
  exports: [CMSLoaderComponent]
})
export class WagtailModule {
  static forRoot(wagtailConfig: IWagtailModuleConfig): ModuleWithProviders<WagtailModule> {
    return {
      ngModule: WagtailModule,
      providers: [
        CoalescingComponentFactoryResolver,
        WagtailService,
        StatusCodeService,
        { provide: WAGTAIL_CONFIG, useValue: wagtailConfig }
      ]
    };
  }

  // forFeature idea from ngrx
  static forFeature(pageTypes: IPageTypes[]): ModuleWithProviders<FeatureRoutesModule> {
    return {
      ngModule: FeatureRoutesModule,
      providers: [
        { provide: FEATURE_PAGE_TYPES, useValue: pageTypes, multi: true }
      ]
    };
  }

  constructor(coalescingResolver: CoalescingComponentFactoryResolver) {
    coalescingResolver.init();
  }
}
