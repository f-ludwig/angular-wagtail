import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  CMSLoaderGuard,
  CMSLoaderComponent
} from "../../projects/angular-wagtail/src/public-api";

const routes: Routes = [
  {
    path: "simple-lazy",
    loadChildren: () =>
      import("./simple-lazy/simple-lazy.module").then(m => m.SimpleLazyModule)
  },
  {
    path: "preview",
    loadChildren: () => import("../../projects/angular-wagtail/src/lib/preview/preview.module").then(m => m.PreviewModule)
  },
  { path: "**", component: CMSLoaderComponent, canActivate: [CMSLoaderGuard] }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      initialNavigation: "enabled",
      relativeLinkResolution: "corrected"
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
