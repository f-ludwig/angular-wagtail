import {
  BrowserModule,
  BrowserTransferStateModule
} from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";

import { AppComponent } from "./app.component";
import { FooComponent } from "./foo/foo.component";
import { NotFoundComponent } from "./not-found/not-found.component";
import { AppRoutingModule } from "./app-routing.module";
import { HomeModule } from "./home/home.module";
import { WagtailModule } from "projects/angular-wagtail/src/public-api";

@NgModule({
  declarations: [AppComponent, FooComponent, NotFoundComponent],
  entryComponents: [FooComponent, NotFoundComponent],
  imports: [
    BrowserModule.withServerTransition({ appId: "app-root" }),
    HomeModule,
    WagtailModule.forRoot({
      wagtailSiteDomain: "http://localhost:8000",
      wagtailSiteId: 2,
      pageTypes: [
        {
          type: "sandbox.FooPage",
          loadChildren: () =>
            import("./lazy/lazy.module").then(m => m.LazyModule)
        },
        {
          type: "sandbox.BarPage",
          loadChildren: () =>
            import("./lazy/lazy.module").then(m => m.LazyModule)
        }
      ]
    }),
    AppRoutingModule,
    HttpClientModule,
    BrowserTransferStateModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
