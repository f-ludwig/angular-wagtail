import { Component, OnInit, Input } from "@angular/core";
import {
  WagtailService,
  IWagtailPageDetail
} from "../../../projects/angular-wagtail/src/public-api";
import { tap } from "rxjs/operators";

@Component({
  selector: "app-foo",
  template: `
    <h2>Foo Component</h2>
    <div>{{ cmsData?.title }}</div>
    title is: {{ title }}
  `,
  styleUrls: ["./foo.component.css"]
})
export class FooComponent implements OnInit {
  @Input() title: string;
  cmsData: IWagtailPageDetail;
  constructor(private wagtailService: WagtailService) {}

  ngOnInit() {
    this.wagtailService
      .getPageForCurrentUrl()
      .pipe(tap(page => (this.cmsData = page)))
      .toPromise();
  }
}
