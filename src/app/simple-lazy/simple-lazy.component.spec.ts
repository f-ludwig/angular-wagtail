import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SimpleLazyComponent } from './simple-lazy.component';

describe('SimpleLazyComponent', () => {
  let component: SimpleLazyComponent;
  let fixture: ComponentFixture<SimpleLazyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SimpleLazyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimpleLazyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
